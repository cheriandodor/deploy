import imaplib
import email
from email.header import decode_header
import os
import logging

# Set up logging configuration
logging.basicConfig(filename='/app/cronjob.log',
                    level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(message)s')

# Read IMAP server credentials from environment variables
IMAP_SERVER = os.getenv("IMAP_SERVER")
EMAIL_USER = os.getenv("EMAIL_USER")
EMAIL_PASS = os.getenv("EMAIL_PASS")
IMAP_PORT = os.getenv("IMAP_PORT", 993)  # Default to 993 if not specified

if not IMAP_SERVER or not EMAIL_USER or not EMAIL_PASS:
    raise EnvironmentError("IMAP_SERVER, EMAIL_USER, and EMAIL_PASS must be set as environment variables.")

# Connect to IMAP server
def connect_imap():
    try:
        mail = imaplib.IMAP4_SSL(IMAP_SERVER, IMAP_PORT)
        mail.login(EMAIL_USER, EMAIL_PASS)
        logging.info("Connected to IMAP server successfully.")
        return mail
    except Exception as e:
        logging.error(f"Failed to connect to IMAP server: {e}")
        raise

# Decode email header
def decode_mime_words(s):
    return ''.join(
        word.decode(enc or 'utf-8') if isinstance(word, bytes) else word
        for word, enc in email.header.decode_header(s)
    )

# Convert to CamelCase
def to_camel_case(s):
    return ''.join(word.capitalize() for word in s.split('.'))

# Create necessary folders based on the email address
def create_folders(mail, parts):
    base_path = to_camel_case(parts[0])
    try:
        mail.create(base_path)
        logging.info(f"Folder created: {base_path}")
    except Exception as e:
        logging.error(f"Error creating folder '{base_path}': {e}")

    for i in range(1, len(parts)):
        next_folder = to_camel_case(parts[i])
        base_path = f"{base_path}/{next_folder}"
        try:
            mail.create(base_path)
            logging.info(f"Folder created: {base_path}")
        except Exception as e:
            logging.error(f"Error creating folder '{base_path}': {e}")

    return base_path

# Process emails in the INBOX
def process_emails(mail):
    mail.select("INBOX")
    status, messages = mail.search(None, 'ALL')
    
    if status != 'OK':
        logging.warning("No emails found.")
        return

    # Fetch and process each email
    for num in messages[0].split():
        status, msg_data = mail.fetch(num, '(RFC822)')
        if status != 'OK':
            logging.error("Failed to fetch email.")
            continue

        msg = email.message_from_bytes(msg_data[0][1])
        to_header = msg.get("To", "")

        # Check if the email is read
        typ, msg_flags = mail.fetch(num, "(FLAGS)")
        is_read = b'\\Seen' in msg_flags[0]

        # Parse the recipient email
        if to_header:
            recipient_email = email.utils.parseaddr(to_header)[1]
            # Check if the email matches your subdomain pattern
            if recipient_email.endswith(('@pk.pushpakravitej.com', '@pk.pkdb.io', '@pk.pkdb.org', '@binengg.com')):
                local_part = recipient_email.split('@')[0]
                parts = local_part.split('.')

                # Handle cases with one part or multiple parts
                if len(parts) >= 1:
                    folder_path = create_folders(mail, parts)

                    # Move email to the new folder
                    result = mail.copy(num, folder_path)
                    if result[0] == 'OK':
                        logging.info(f"Email {num} moved to folder: {folder_path}")
                        # If the email is read, remove the INBOX label
                        if is_read:
                            mail.store(num, '-X-GM-LABELS', 'INBOX')
                            logging.info(f"Removed INBOX label from email: {recipient_email}")

# Main function to run
def main():
    mail = connect_imap()
    process_emails(mail)
    mail.logout()
    logging.info("Logged out from IMAP server.")

if __name__ == "__main__":
    logging.info("Starting")
    main()
