services:
  # This service runs the postgres DB used by dagster for run storage, schedule storage,
  # and event log storage.
  dagster_postgresql:
    image: postgres:11
    container_name: dagster_postgresql
    restart: always
    environment:
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - POSTGRES_DB=${POSTGRES_DB}
      - PGDATA=${PGDATA}
    networks:
      dagster_network:
    volumes:
      - /mnt/supertank/docker/postgresql/dagster/data:/var/lib/postgresql/data
      - /mnt/supertank/docker/postgresql/db_dump/dagster:/dump
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 10s
      timeout: 5s
      retries: 5

  # This service runs dagster-webserver, which loads your user code from the user code container.
  # Since our instance uses the QueuedRunCoordinator, any runs submitted from the webserver will be put on
  # a queue and later dequeued and launched by dagster-daemon.
  dagster_webserver:
    build:
      context: .
      dockerfile: ./Dockerfile_dagster
    restart: always
    entrypoint:
      - dagster-webserver
      - -h
      - "0.0.0.0"
      - -p
      - "3000"
      - -w
      - workspace.yaml
    container_name: dagster_webserver
    # expose:
    #   - "3000"
    # ports:
    #   - "3000:3000"
    environment:
      - DAGSTER_POSTGRES_USER=${POSTGRES_USER}
      - DAGSTER_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - DAGSTER_POSTGRES_DB=${POSTGRES_DB}
    volumes: # Make docker client accessible so we can terminate containers from the webserver
      - /var/run/docker.sock:/var/run/docker.sock
      - /mnt/supertank/docker/dagster/tmp:/tmp/io_manager_storage
    networks:
      dagster_network:
      dkr:
        ipv4_address: 172.21.11.144
    depends_on:
      dagster_postgresql:
        condition: service_healthy
      dagster_teslacam:
        condition: service_started
      dagster_meltano:
        condition: service_started

  # This service runs the dagster-daemon process, which is responsible for taking runs
  # off of the queue and launching them, as well as creating runs from schedules or sensors.
  dagster_daemon:
    build:
      context: .
      dockerfile: ./Dockerfile_dagster
    entrypoint:
      - dagster-daemon
      - run
    container_name: dagster_daemon
    restart: always
    environment:
      - DAGSTER_POSTGRES_USER=${POSTGRES_USER}
      - DAGSTER_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - DAGSTER_POSTGRES_DB=${POSTGRES_DB}
    volumes: # Make docker client accessible so we can launch containers using host docker
      - /var/run/docker.sock:/var/run/docker.sock
      - /mnt/supertank/docker/dagster/tmp:/tmp/io_manager_storage
    networks:
      dagster_network:
    depends_on:
      dagster_postgresql:
        condition: service_healthy
      dagster_teslacam:
        condition: service_started
      dagster_meltano:
        condition: service_started

  # This service runs the gRPC server that loads your user code, in both dagster-webserver
  # and dagster-daemon. By setting DAGSTER_CURRENT_IMAGE to its own image, we tell the
  # run launcher to use this same image when launching runs in a new container as well.
  # Multiple containers like this can be deployed separately - each just needs to run on
  # its own port, and have its own entry in the workspace.yaml file that's loaded by the
      # webserver.
  # dagster_example:
  #   build:
  #     context: ./example
  #     dockerfile: ./Dockerfile_user_code
  #   container_name: dagster_example
  #   image: dagster_example_image
  #   restart: always
  #   environment:
  #     - DAGSTER_POSTGRES_USER=${POSTGRES_USER}
  #     - DAGSTER_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
  #     - DAGSTER_POSTGRES_DB=${POSTGRES_DB}
  #     - DAGSTER_CURRENT_IMAGE=dagster_example_image
  #   networks:
  #     dagster_network:

  dagster_teslacam:
    build:
      context: ./teslacam
      dockerfile: ./Dockerfile_user_code
    container_name: dagster_teslacam
    image: dagster_teslacam_image
    restart: always
    environment:
      - DAGSTER_POSTGRES_USER=${POSTGRES_USER}
      - DAGSTER_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - DAGSTER_POSTGRES_DB=${POSTGRES_DB}
      - DAGSTER_CURRENT_IMAGE=dagster_teslacam_image
      - TZ=America/Los_Angeles
    volumes:
      - /mnt/gdrive/TeslaCam:/mnt/src
      - /mnt/tank/mergedteslacam/dest:/mnt/dest
      - /mnt/tank/mergedteslacam/temp:/mnt/temp
      - /mnt/tank/mergedteslacam/archive:/mnt/archive
    networks:
      dagster_network:
    depends_on:
      dagster_postgresql:
        condition: service_healthy

  dagster_meltano:
    build:
      context: ./meltano
      dockerfile: ./Dockerfile_user_code
    container_name: dagster_meltano
    image: dagster_meltano_image
    restart: always
    environment:
      - DAGSTER_POSTGRES_USER=${POSTGRES_USER}
      - DAGSTER_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - DAGSTER_POSTGRES_DB=${POSTGRES_DB}
      - DAGSTER_CURRENT_IMAGE=dagster_teslacam_image
      - TZ=America/Los_Angeles
    # volumes:
    #   - /mnt/gdrive/TeslaCam:/mnt/src
    #   - /mnt/tank/mergedteslacam/dest:/mnt/dest
    #   - /mnt/tank/mergedteslacam/temp:/mnt/temp
    #   - /mnt/tank/mergedteslacam/archive:/mnt/archive
    networks:
      dagster_network:
      dkr:
        ipv4_address: 172.21.11.18
    depends_on:
      dagster_postgresql:
        condition: service_healthy

networks:
  dagster_network:
    driver: bridge
    name: dagster_network
    internal: true
    ipam:
      config:
        - subnet: 10.1.68.0/24
  dkr:
    name: br0.1121
    external: true
