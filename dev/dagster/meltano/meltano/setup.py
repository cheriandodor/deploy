from setuptools import find_packages, setup

setup(
    name="meltano",
    packages=find_packages(exclude=["meltano_tests"]),
    install_requires=[
        "dagster",
        "dagster-cloud",
        "dagster-meltano"
    ],
    extras_require={"dev": ["dagster-webserver", "pytest"]},
)
