# from dagster import repository, job, Definitions
# from dagster_meltano import load_jobs_from_meltano_project, meltano_resource, meltano_command_op, meltano_run_op
# import os

# # cwd = os.getcwd()

# # meltano_jobs = load_jobs_from_meltano_project(f"{cwd}/projects/pkdb")

# # @job(resource_defs={"meltano": meltano_resource})
# # def meltano_command_job():
# #     meltano_command_op("install loader tap-smoke-test")()

# # @repository
# # def repository():
# #     return [meltano_jobs]


# # @job(resource_defs={"meltano": meltano_resource})
# # def run_elt_job():
# # #    tap_done = meltano_run_op("run ha-postgres-extractor ha-postgres-target")()
# #    tap_done = meltano_command_op("config ha-postgres-extractor")()
# #    return True

# # defs = Definitions(jobs=[run_elt_job])

# @job(resource_defs={"meltano": meltano_resource})
# def meltano_command_job():
#     meltano_command_op("install loader tap-smoke-test")()
#     a = 3

# @repository()
# def repository():
#     return [meltano_command_job]


## Another example 

# ref: https://github.com/quantile-development/dagster-meltano/issues/28#issuecomment-1655283987



import os
from pathlib import Path

from dagster import (
    repository,
    with_resources,
    job,
    asset,
    op,
    OpExecutionContext,
    resource,
    InitResourceContext
)

from dagster_meltano import (
    load_assets_from_meltano_project,
    load_jobs_from_meltano_project,
    meltano_resource,
    meltano_command_op,
)

cwd = os.getcwd()

MELTANO_PROJECT_DIR = f"{cwd}/projects/pkdb"
# MELTANO_BIN = os.getenv("MELTANO_BIN", "meltano")

meltano_jobs = load_jobs_from_meltano_project(MELTANO_PROJECT_DIR)

# @resource
# def run_id_resource(init_context: InitResourceContext):
#     init_context.log.info("Hello, world!")
#     a = init_context.run_id
#     return init_context.run_id

@job(resource_defs={"meltano": meltano_resource})
def run_elt_job():
    meltano_command_op("config ha-postgres-extractor list", "command_name")()

# @op()
# def get_run_id(context: OpExecutionContext):
#     return context.run.run_id

@job(description="Transfer HA DB data to PKDB DB", resource_defs={"meltano": meltano_resource})
def ha_transfer():
    meltano_command_op(f"run ha-postgres-extractor ha-postgres-target", "transfer_ha_pkdb")()

# @asset(resource_defs={"meltano": meltano_resource})
# def check_config(context):
# #    tap_done = meltano_run_op("run ha-postgres-extractor ha-postgres-target")()
#     tap_done = meltano_command_op("config ha-postgres-extractor list")()
#     return tap_done
# #    return True


@repository
def meltano():
    return [
        # meltano_jobs,
        run_elt_job,
        ha_transfer,
        # check_config,
        with_resources(
            load_assets_from_meltano_project(
                meltano_project_dir=MELTANO_PROJECT_DIR,
            ),
            {
                "meltano": meltano_resource,
            },
        )
    ]
