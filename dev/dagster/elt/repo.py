from dagster_embedded_elt.sling import (
    SlingConnectionResource,
    SlingResource,
    sling_assets,
)

from dagster import EnvVar

ha_source = SlingConnectionResource(
    name="HomeAssistant_Source",
    type="postgres",
    host=EnvVar("PKDB_PG_PASS"),
    port=5432,
    database=EnvVar("PKDB_PG_PASS"),
    user=EnvVar("PKDB_PG_PASS"),
    password=EnvVar("PKDB_PG_PASS"),
    schema="public",
)

tm_source = SlingConnectionResource(
    name="Teslamate_Source",
    type="postgres",
    host=EnvVar("HA_PG_PASS"),
    port=5432,
    database=EnvVar("HA_PG_PASS"),
    user=EnvVar("HA_PG_PASS"),
    password=EnvVar("HA_PG_PASS"),
    schema="public",
)

target = SlingConnectionResource(
    name="PKDB",
    type="postgres",
    host=EnvVar("PKDB_PG_PASS"),
    port=5432,
    database=EnvVar("PKDB_PG_PASS"),
    user=EnvVar("PKDB_PG_PASS"),
    password=EnvVar("PKDB_PG_PASS"),
)

sling = SlingResource(
    connections=[
        source,
        target,
    ]
)

replication_config_ha = {
    "SOURCE": "HomeAssistant_Source",
    "TARGET": "PKDB",
    "defaults": {
        "mode": "incremental",
        "object": "{target_schema}.{stream_schema}_{stream_table}",
    },
    "streams": {
        "public.*": None,
    },
}

replication_config_tm = {
    "SOURCE": "Teslamate_Source",
    "TARGET": "PKDB",
    "defaults": {
        "mode": "incremental",
        "object": "{target_schema}.{stream_schema}_{stream_table}",
    },
    "streams": {
        "public.*": None,
    },
}


@sling_assets(replication_config=replication_config_ha, name="Home Assistant Copy")
def my_assets(context, sling: SlingResource):
    yield from sling.replicate(context=context)

@sling_assets(replication_config=replication_config_tm, name="Teslamate Copy")
def my_assets(context, sling: SlingResource):
    yield from sling.replicate(context=context)
