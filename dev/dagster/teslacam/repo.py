import os
import urllib.request
import subprocess

from dagster import AssetExecutionContext, DailyPartitionsDefinition, asset, EnvVar, op
from dagster import graph
from dagster_shell import create_shell_command_op, execute_shell_command
import logging


if "DAGSTER_ENV" in os.environ and os.getenv("DAGSTER_ENV") == "dev":
    SOURCE = "/Users/pushpak.ravitej/personal/Teslacam/src"
    DEST = "/Users/pushpak.ravitej/personal/Teslacam/dest"
    ARCHIVE = "/Users/pushpak.ravitej/personal/Teslacam/archive"
    TEMP_DIR = "/Users/pushpak.ravitej/personal/Teslacam/temp"
else:
    SOURCE = "/mnt/src"
    DEST = "/mnt/dest"
    ARCHIVE = "/mnt/archive"
    TEMP_DIR = "/mnt/temp"

print(os.listdir(SOURCE))

# @op
def process_sentry() -> None:
    folder_name = "SentryClips"
    input_path = "/".join([SOURCE, folder_name])
    archive_path = "/".join([ARCHIVE, folder_name])
    output_path = "/".join([DEST, folder_name])
    clips = os.listdir(input_path)
    clips.sort()
    # a()
    for clip in clips:
        if clip.startswith("."):
            continue
        name = clip
        args = [
            "tesla_dashcam",
            "--loglevel DEBUG",
            " ".join(["--temp_dir", TEMP_DIR]),
            "--display_ts",
            # "--skip_existing",
            "--layout DIAMOND",
            "--halign RIGHT",
            "--valign TOP",
            "--text_overlay_fmt '{local_timestamp_rolling}\n{event_city}\n{event_reason}\nlatitude: {event_latitude}, longitude:{event_longitude}'",
            " ".join(["--output", output_path]),
            "--motion_only",
            "--monitor_once '{folder_name}'".format(folder_name=folder_name),
            "--quality HIGH",
            "--monitor_trigger",
            # "/".join([input_path, clip]),
            SOURCE,
        ]
        args2 = [
            "tesla_dashcam",
            "--loglevel DEBUG",
            "--temp_dir /Users/pushpak.ravitej/personal/Teslacam/temp",
            "--display_ts",
            "--layout DIAMOND",
            "--halign RIGHT",
            "--valign TOP",
            "--text_overlay_fmt '{local_timestamp_rolling}\n{event_city}\n{event_reason}\nlatitude: {event_latitude}, longitude:{event_longitude}'",
            "--output /Users/pushpak.ravitej/personal/Teslacam/dest/RecentClips",
            "--motion_only",
            "--monitor_once 'RecentClips'",
            "--quality HIGH",
            "--monitor_trigger"
            "/Users/pushpak.ravitej/personal/Teslacam/src"
        ]
        process = subprocess.Popen(args2, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        # os.system(" ".join(args))
        a = 3

    # Commands that worked: tesla_dashcam --loglevel DEBUG --temp_dir /Users/pushpak.ravitej/personal/Teslacam/temp --display_ts --layout DIAMOND --halign RIGHT --valign TOP --text_overlay_fmt '{local_timestamp_rolling}\n{event_city}\n{event_reason}\nlatitude: {event_latitude}, longitude:{event_longitude}' --output /Users/pushpak.ravitej/personal/Teslacam/dest/RecentClips --motion_only --monitor_once 'RecentClips' --quality HIGH --monitor_trigger /Users/pushpak.ravitej/personal/Teslacam/src


@asset(partitions_def=DailyPartitionsDefinition(start_date="2024-01-01"))
def my_daily_partitioned_asset(context: AssetExecutionContext) -> None:
    partition_date_str = context.partition_key

    url = f"https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date={partition_date_str}"
    target_location = f"nasa/{partition_date_str}.csv"

    urllib.request.urlretrieve(url, target_location)


if "DAGSTER_ENV" in os.environ and os.getenv("DAGSTER_ENV") == "dev":
    process_sentry()
