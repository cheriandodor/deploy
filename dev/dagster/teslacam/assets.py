from dagster import (
    asset,
    DailyPartitionsDefinition,
    Output,
    MetadataValue,
    AssetIn,
    AssetExecutionContext,
    AssetKey,
    DagsterEventType,
    EventRecordsFilter,
    DagsterInstance,
    RunsFilter,
    DagsterRunStatus,
    MultiPartitionsDefinition,
    StaticPartitionsDefinition,
    asset_check,
    AssetCheckResult,
)
import subprocess
import os
from pathlib import Path
from glob import glob
import uuid
import shutil


if "DAGSTER_ENV" in os.environ and os.getenv("DAGSTER_ENV") == "dev":
    SOURCE = "/Users/pushpak.ravitej/personal/Teslacam/src"
    DEST = "/Users/pushpak.ravitej/personal/Teslacam/dest"
    ARCHIVE = "/Users/pushpak.ravitej/personal/Teslacam/archive"
    TEMP_DIR = "/Users/pushpak.ravitej/personal/Teslacam/temp"
else:
    SOURCE = "/mnt/src"
    DEST = "/mnt/dest"
    ARCHIVE = "/mnt/archive"
    TEMP_DIR = "/mnt/temp"
FOLDER_NAMES = {
    "SentryClips": {"single_folder": False},
    "RecentClips": {"single_folder": True},
    "SavedClips": {"single_folder": False},
}
Path(DEST).mkdir(parents=True, exist_ok=True)
Path(ARCHIVE).mkdir(parents=True, exist_ok=True)
Path(TEMP_DIR).mkdir(parents=True, exist_ok=True)

'''
Some helpful links
    - https://github.com/dagster-io/dagster/discussions/17956
    - https://github.com/dagster-io/dagster/discussions/14622
    - https://github.com/dagster-io/dagster/discussions/15532
'''
# We can also create multidimnesional partitions
# @asset(
#     partitions_def=MultiPartitionsDefinition(
#         {
#             "date": DailyPartitionsDefinition(start_date="2022-01-01"),
#             "color": StaticPartitionsDefinition(["red", "yellow", "blue"]),
#         }
#     )
# )+

partitions_def=MultiPartitionsDefinition(
    {
        "car": StaticPartitionsDefinition(["ujala", "nirma"]),
        "date": DailyPartitionsDefinition(start_date="2022-01-01"),
        # "event_type": StaticPartitionsDefinition(["RecentClips", "SavedClips", "SentryClips", "TeslaTrackMode"]),
    }
)

def get_latest_materializations_for_partition(instance: DagsterInstance, asset_key: AssetKey, partition_key: str):
	# Create an EventRecordsFilter for the asset_key and partition_key
	event_records_filter = EventRecordsFilter(
		event_type=DagsterEventType.ASSET_MATERIALIZATION,
		asset_key=asset_key,
		asset_partitions=[partition_key],
	)
	# Fetch the event records
	event_records = instance.get_event_records(
		filters=event_records_filter,
		ascending=False,# Ensure we get the latest event first
	)
	# Get the latest materialization event from the records
	return event_records

def get_latest_run_by_id(instance: DagsterInstance, run_id: str):
    run_records_filtedr = RunsFilter(
        run_ids=[run_id]
    )

    run_record = instance.get_run_by_id(run_id=run_id)
    return run_record

def get_runs(instance: DagsterInstance, status: DagsterRunStatus):
    filter = RunsFilter(
        statuses=[status]
    )
    return instance.get_run_records(
        filters=filter,
        order_by="end_time",
        ascending=False
    )

# def copy_files_fpr_processing()


# partitions_def = DailyPartitionsDefinition(start_date="2020-01-01")

@asset(partitions_def=partitions_def, output_required=False)
def check_footage_recentclips(context):# -> Output[str]:
    partition_key_str = context.asset_partition_key_for_output()
    car_name = partition_key_str.split("|")[0]
    date = partition_key_str.split("|")[1]
    uid = context.run.run_id
    working_folder = f"{TEMP_DIR}/{uid}"
    result = {}
    for folder_name, metadata in FOLDER_NAMES.items():
        single_folder = metadata.get("single_folder", True)
        context.log.info(f"looking for files in {SOURCE}/{folder_name}")
        footage_base_path = f"{SOURCE}/{car_name}/{folder_name}"
        source_files_tmp = glob(f"{footage_base_path}/**/{date}*.mp4", recursive=True)
        if len(source_files_tmp) > 0:
            temp_folder = f"{working_folder}/{folder_name}"
            Path(temp_folder).mkdir(exist_ok=True, parents=True)
            if single_folder:
                for file in source_files_tmp:
                    folder_path = os.path.dirname(file)
                    shutil.copy2(file, temp_folder)
                output_path = f"{ARCHIVE}/{car_name}/{date}/{folder_name}"
                if os.path.isdir(output_path):
                    processed_files = glob(f"{output_path}/**/*", recursive=True)
                    if len(processed_files) > 0:
                        for processed_file in processed_files:
                            name = os.path.basename(processed_file)
                            shutil.move(processed_file, os.path.join(temp_folder, name))
                remove_hidden_files_and_folders(output_path)
                if os.path.isdir(output_path) and len(os.listdir(output_path)) == 0:
                    os.rmdir(output_path)
            else:
                folder_paths = []
                for file in source_files_tmp:
                    folder_path = os.path.dirname(file)
                    if folder_path not in folder_paths:
                        folder_paths.append(folder_path)
                for folder_path in folder_paths:
                    dest_folder = f"{temp_folder}/{Path(folder_path).name}"
                    Path(dest_folder).mkdir(exist_ok=True, parents=True)
                    files = glob(f"{folder_path}/**/*", recursive=True)
                    for file in files:
                        shutil.copy2(file, dest_folder)
                    output_path = f"{ARCHIVE}/{car_name}/{date}/{folder_name}/{Path(folder_path).name}"
                    if os.path.isdir(output_path):
                        processed_files = glob(f"{output_path}/**/*", recursive=True)
                        if len(processed_files) > 0:
                            for processed_file in processed_files:
                                name = os.path.basename(processed_file)
                                shutil.move(processed_file, os.path.join(dest_folder, name))
                    remove_hidden_files_and_folders(output_path)
                    if os.path.isdir(output_path) and len(os.listdir(output_path)) == 0:
                        os.rmdir(output_path)
            result.update(
                {
                    folder_name: {
                        "metadata": metadata,
                        "source_files": source_files_tmp,
                        "working_files": glob(f"{temp_folder}/**/{date}*.mp4", recursive=True),
                    }
                }
            )
    context.log.info(
        {
            "message": f"Files copied to working folder ({working_folder})",
            "partition": context.asset_partition_key_for_output(),
            "car": car_name,
            "date": date,
            "working_folder": working_folder,
            "result": result,
        }
    )
    yield Output(
        {
            "working_folder": working_folder,
            "result": result,
        },
        metadata={
            "partition": context.asset_partition_key_for_output(),
            "car": car_name,
            "date": date,
            "working_folder": MetadataValue.path(working_folder),
            "result": result,
        }
    )

@asset(partitions_def=partitions_def)
def process_footage_recentclips(context, check_footage_recentclips):# -> Output[str]:
    # Update to use result
    passed = True
    partition_key_str = context.asset_partition_key_for_output()
    car_name = partition_key_str.split("|")[0]
    date = partition_key_str.split("|")[1]
    uid = context.run.run_id
    working_folder = check_footage_recentclips["working_folder"]
    result = check_footage_recentclips["result"]
    for folder_name, metadata in FOLDER_NAMES.items():
        if folder_name not in result:
            continue
        output_path = f"{DEST}/{car_name}/{date}/{folder_name}"
        process_path = f"{working_folder}"
        os.makedirs(os.path.dirname(output_path), exist_ok=True)
        context.log.info(
            {
                "message": f"Processing footage in {working_folder}/{folder_name}",
                "partition": context.asset_partition_key_for_output(),
            }
        )
        args = [
            'tesla_dashcam',
            # '--loglevel',
            # 'DEBUG',
            '--no-check_for_update',
            '--no-gpu',
            '--temp_dir',
            # '/Users/pushpak.ravitej/personal/Teslacam/temp',
            TEMP_DIR,
            '--display_ts',
            '--layout',
            'CROSS',
            '--halign',
            'RIGHT',
            '--valign',
            'TOP',
            '--text_overlay_fmt',
            '{local_timestamp_rolling}\\n{event_city}\\n{event_reason}\\nlatitude: {event_latitude}, longitude:{event_longitude}',
            '--output',
            # '/Users/pushpak.ravitej/personal/Teslacam/dest/RecentClips',
            output_path,
            '--motion_only',
            '--monitor_once',
            # 'RecentClips',
            folder_name,
            '--quality',
            'HIGH',
            '--encoding',
            'x265',
            # '--compression',
            # 'veryslow',
            '--monitor_trigger',
            # '/Users/pushpak.ravitej/personal/Teslacam/src'
            process_path,
        ]
        process = subprocess.Popen(args=args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Capture stdout and stderr output
        stdout, stderr = process.communicate()

        # Decode byte data to string
        stdout_str = stdout.decode()
        stderr_str = stderr.decode()
        
        return_code = process.returncode
        result[folder_name].update(
            {
                "stdout_str": stdout_str,
                "stderr_str": stderr_str,
                "return_code": return_code,
                "output_path": output_path,
            }
        )
        log = {
            "partition": context.asset_partition_key_for_output(),
            "folder_name": folder_name,
            "source_files": result[folder_name]["source_files"],
            "working_folder": MetadataValue.path(working_folder),
            "working_files": result[folder_name]["working_files"],
            "stdout_str": stdout_str,
            "stderr_str": stderr_str,
            "return_code": return_code,
            "output_path": output_path,
        }
        context.log.info(log)
        if return_code != 0 or stderr_str != '':
            passed = False
    if passed:
        yield Output(
            {
                "working_folder": working_folder,
                "result": result,
            },
            metadata={
                "partition": context.asset_partition_key_for_output(),
                "car": car_name,
                "date": date,
                "working_folder": MetadataValue.path(working_folder),
                "result": result,
            }            
        )

@asset_check(asset=process_footage_recentclips, description="Check for processing errors")
def process_footage_recentclips_check(context, asset) -> AssetCheckResult:
    context.log.info("checking if processing was successful")
    passed = True
    result = asset["result"]
    for folder_name, metadata in FOLDER_NAMES.items():
        if folder_name not in result:
            continue
        if not result[folder_name]["return_code"] == 0 or not result[folder_name]["stderr_str"] == '':
            context.log.info.error(
                {
                    "message": f"Failure in {folder_name}",
                    "partition": context.asset_partition_key_for_output(),
                    "folder_name": folder_name,
                    "return_code": result[folder_name]["return_code"],
                    "stderr_str": result[folder_name]["stderr_str"]
                }
            )
            passed = False
    return AssetCheckResult(passed=passed, metadata={"result": result})

@asset_check(asset=process_footage_recentclips, description="Check for processing errors")
def process_footage_recentclips_check(process_footage_recentclips) -> AssetCheckResult:
    context.log.info("checking if processing was successful")
    passed = True
    result = process_footage_recentclips["result"]
    for folder_name, metadata in FOLDER_NAMES.items():
        if folder_name not in result:
            continue
        if not result[folder_name]["return_code"] == 0 or not result[folder_name]["stderr_str"] == '':
            log.info.error(
                {
                    "message": f"Failure in {folder_name}",
                    "partition": context.asset_partition_key_for_output(),
                    "folder_name": folder_name,
                    "return_code": result[folder_name]["return_code"],
                    "stderr_str": result[folder_name]["stderr_str"]
                }
            )
            passed = False
    return AssetCheckResult(passed=passed, metadata={"result": result})

def remove_hidden_files_and_folders(directory):
    for root, dirs, files in os.walk(directory, topdown=False):
        for name in dirs:
            if name.startswith('.'):
                hidden_folder_path = os.path.join(root, name)
                os.rmdir(hidden_folder_path)
        for name in files:
            if name.startswith('.'):
                hidden_file_path = os.path.join(root, name)
                os.remove(hidden_file_path)

@asset(partitions_def=partitions_def, output_required=False)
def cleanup_footage_recentclips(context, process_footage_recentclips):
    partition_key_str = context.asset_partition_key_for_output()
    car_name = partition_key_str.split("|")[0]
    date = partition_key_str.split("|")[1]
    uid = context.run.run_id
    result = process_footage_recentclips["result"]
    for folder_name, metadata in FOLDER_NAMES.items():
        working_folder = f"{process_footage_recentclips["working_folder"]}/{folder_name}"
        if folder_name not in result:
            continue
        if result[folder_name]["return_code"] == 0 and result[folder_name]["stderr_str"] == '':
            source_files = result[folder_name]["source_files"]
            working_files = result[folder_name]["working_files"]
            output_path = f"{ARCHIVE}/{car_name}/{date}/{folder_name}"
            Path(output_path).mkdir(exist_ok=True, parents=True)
            context.log.info(f"Moving files from {working_folder} to {output_path}")
            shutil.copytree(working_folder, output_path, dirs_exist_ok=True)
            shutil.rmtree(working_folder)
            context.log.info(f"Deleting Source files")
            source_path_list = []
            for source_file in source_files:
                os.remove(source_file)
                source_dir = os.path.dirname(source_file)
                if source_dir not in source_path_list:
                    source_path_list.append(source_dir)
            context.log.info(f"Deleting Source directories if empty")
            for source_path_dir in source_path_list:
                context.log.info(
                    {
                        "message": f"Cleaning up {source_path_dir}",
                    }
                )
                shutil.rmtree(source_path_dir)
    yield Output(
        {
            "working_folder": working_folder,
            "result": result,
        },
        metadata={
            "partition": context.asset_partition_key_for_output(),
            "car": car_name,
            "date": date,
            "working_folder": MetadataValue.path(working_folder),
            "result": result,
        }            
    )
