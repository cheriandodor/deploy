from dagster import (
    RunRequest,
    load_assets_from_modules,
    define_asset_job,
    repository,
    schedule,
    DefaultScheduleStatus,
    load_asset_checks_from_package_module,
    AssetChecksDefinition
)
import inspect
from datetime import (
    datetime,
    timedelta,
)
import assets

teslacam_assets = load_assets_from_modules([assets])

def load_asset_checks(module):
    asset_checks = []
    for name, obj in inspect.getmembers(module):
        if isinstance(obj, AssetChecksDefinition):
            asset_checks.append(obj)
    return asset_checks

teslacam_asset_checks = load_asset_checks(assets)

teslacam_job = define_asset_job("teslacam_job")

def last_10_days_partition_selector(now=datetime.today()):
    partitions = []
    cars = ["ujala", "nirma"]
    for c in cars:
        for i in range(2, 12):
            partitions.append(f"{c}|{(now - timedelta(days=i)).strftime("%Y-%m-%d")}")
    return partitions

@schedule(
    cron_schedule="0 0 * * *",
    job=teslacam_job,
    default_status=DefaultScheduleStatus.STOPPED,
    execution_timezone="America/Los_Angeles"
)  # Runs daily at midnight
def my_asset_schedule(context):
    now = context.scheduled_execution_time
    return [
        RunRequest(
            partition_key=partition_key,
            run_key=f"{context.scheduled_execution_time}_{partition_key}"
        )
        for partition_key in last_10_days_partition_selector(now)
    ]


@repository
def teslacam_repository():
    # return [teslacam_job, my_asset_schedule, teslacam_assets, teslacam_asset_checks]
    return [teslacam_job, my_asset_schedule, teslacam_assets]
