# # io_manager.py
# from dagster import io_manager, IOManager, IOManagerContext
# import os

# class LocalFileIOManager(IOManager):
#     def handle_output(self, context: IOManagerContext, obj):
#         base_path = context.resource_config["base_path"]
#         file_path = os.path.join(base_path, context.asset_key.path[-1], context.partition_key)
#         os.makedirs(os.path.dirname(file_path), exist_ok=True)
#         with open(file_path, "w") as f:
#             f.write(obj)
#         context.log.info(f"Saved output to {file_path}")

#     def load_input(self, context: IOManagerContext):
#         base_path = context.resource_config["base_path"]
#         file_path = os.path.join(base_path, context.asset_key.path[-1], context.partition_key)
#         with open(file_path, "r") as f:
#             return f.read()

# @io_manager(config_schema={"base_path": str})
# def local_file_io_manager(init_context):
#     return LocalFileIOManager()
