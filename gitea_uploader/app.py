from rocketry import Rocketry
from constants import *
from gitea_extender import SuperGitea

app = Rocketry()

@app.task('every 60 seconds')
def run_daily():
    print("Syncing!")
    if len(gitea):
        for git in gitea:
            git.commit_new_files()

if __name__ == "__main__":
    print("starting")
    gitea = []
    for config in gitea_configs:
        gitea.append(
            SuperGitea(
                config.get("url", "https:://localhost:3000"),
                GITEA_TOKEN,
                config["repo"]["owner"],
                config["repo"]["name"],
                config["repo"]["branch"],
                config["default_path"],
                config["monitor_path"],
                config["processed_path"],
            )
        )
    app.run()
