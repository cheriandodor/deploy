# from gitea import *
import giteapy
# import gitea
from giteapy.rest import ApiException
from constants import GITEA_TOKEN
import os
import base64
from datetime import datetime
import json
import shutil
import fnmatch
import time
import hashlib

class SuperGitea():
    def __init__(self, url, GITEA_TOKEN, owner, repo, branch, default_path, monitor_path, processed_path) -> None:
        # self.gitea = gitea.Gitea(url, GITEA_TOKEN)
        # self.pygitea_repo = gitea.Repository.request(self.gitea, owner=owner, name=repo)
        configuration = giteapy.Configuration()
        configuration.api_key['Authorization'] = "token " + GITEA_TOKEN
        configuration.host = url + "/api/v1"
        self.gitea_admin = giteapy.AdminApi(giteapy.ApiClient(configuration))
        self.gitea_repo = giteapy.RepositoryApi(giteapy.ApiClient(configuration))
        self.owner = owner
        self.repo = repo
        self.default_path = default_path
        self.monitor_path = monitor_path
        self.branch = branch
        self.processed_path = processed_path
        self.ignore = self.get_ignore()

    def commit_new_files(self):
        dir = self.monitor_path
        files = os.listdir(dir)
        for ignore in self.ignore:
            files = [n for n in files if not fnmatch.fnmatch(n, ignore)]
        for filename in files:
            encoded = self.read_and_encode_file(dir + "/" + filename)
            if encoded is not None:
                name = os.uname().nodename
                time = datetime.now().astimezone().strftime("%Y-%m-%d %H:%M:%S %Z")
                message = name + ": " + time
                body = {
                    "content": encoded,
                    "message": message
                }
                # body = giteapy.CreateFileOptions(
                #     content = encoded,
                #     message = message
                # )
                success = False
                while not success:
                    success = self.sync_file(time, message, body, filename)
                self.post_commit_handle(filename, time)

    def sync_file(self, time, message, body, filename):
        success = False
        filepath = self.default_path + "/" + filename
        filepath_out = self.monitor_path + "/" + str(int(datetime.timestamp(datetime.strptime(time, "%Y-%m-%d %H:%M:%S %Z").astimezone()))) + "_" + filename
        filepath_in = self.monitor_path + "/" + filename
        try:
            resp = self.gitea_repo.repo_create_file(self.owner, self.repo, filepath, body)
            # resp = self.pygitea_repo.create_file(filepath, body.content, {
            #     "owner": self.owner,
            #     "message": body.message,
            #     "content": body.content
            # })
        except ApiException as e:
            if "repository file already exists" in e.body:
                # file = self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data.decode('ISO-8859-1')
                if "version https://git-lfs.github.com/spec/v1\noid sha256:" in self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data.decode('utf-8'):
                    filepath_out = self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data.decode('utf-8').split("\n")[1].split(":")[1]
                    if self.compare_files(filepath_in, filepath_out, hash=True):
                        return True
                with open(filepath_out, 'wb') as file:
                    file.write(self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data)
                if self.compare_files(filepath_in, filepath_out):
                    return True
                filepath = self.default_path + "/" + time + " " + filename
                resp = self.gitea_repo.repo_create_file(self.owner, self.repo, filepath, body)
        # Download File
        if "version https://git-lfs.github.com/spec/v1\noid sha256:" in self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data.decode('utf-8'):
            filepath_out = self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data.decode('utf-8').split("\n")[1].split(":")[1]
            if self.compare_files(filepath_in ,filepath_out, hash=True):
                success = True
        else:
            with open(filepath_out, 'wb') as file:
                file.write(self.gitea_repo.repo_get_raw_file(self.owner, self.repo, filepath).data)
            if self.compare_files(filepath_in ,filepath_out):
                success = True
        return success

    def compare_files(self, filepath_in ,filepath_out, hash=False):
        match = False
        with open(filepath_in, "rb") as file:
            hash_in = hashlib.sha256(file.read()).hexdigest()
        if not hash:
            with open(filepath_out, "rb") as file:
                hash_out = hashlib.sha256(file.read()).hexdigest()
                os.remove(filepath_out)
        else:
            hash_out = filepath_out
        if hash_in == hash_out:
            match = True
        return match

    def get_ignore(self):
        ignore = base64.b64decode(self.gitea_repo.repo_get_contents(self.owner, self.repo, ".gitignore").content.encode('utf-8')).decode('utf-8').split("\n")
        # self.gitea.get_file_content({"path": ".gitignore"})
        return ignore

    def post_commit_handle(self, filename, time):
        shutil.move(self.monitor_path + "/" + filename, self.processed_path + "/" + str(int(datetime.timestamp(datetime.strptime(time, "%Y-%m-%d %H:%M:%S %Z").astimezone()))) + "_" + filename)

    def read_and_encode_file(self, path):
        encoded_string = None
        success = False
        while not success:
            try:
                with open(path, "rb") as file:
                    encoded = base64.encodebytes(file.read())
                encoded_string = encoded.decode('utf-8')
            except Exception as e:
                return None
            time.sleep(5)
            try:
                with open(path, "rb") as file:
                    encoded_2 = base64.encodebytes(file.read())
                encoded_string_2 = encoded_2.decode('utf-8')
            except Exception as e:
                return None
            if encoded_string == encoded_string_2:
                success = True
        return encoded_string
