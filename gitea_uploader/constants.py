import os
import yaml

with open('config.yml', 'r') as file:
    config = yaml.safe_load(file)

backend = config.get("backend", {})

gitea_configs = []

for key, val in backend.items():
    if val.get("type") == "gitea":
        gitea_configs.append(backend[key])

GITEA_TOKEN = os.getenv('GITEA_API')
