# Docker Compose Files

- Docker Infrastructure Compose
- Documentation Hub

# Debug docusyncer
`docker-compose -f "gitea_uploader/debug.yml" up -d --build`

# IP Address

## Zones

| CIDR         | Start     | End       | Notes              |
| ------------ | --------- | --------- | ------------------ |
| x.x.x.0/26   | x.x.x.0   | x.x.x.63  | WAN Gateway        |
| x.x.x.64/26  | x.x.x.64  | x.x.x.127 | Vancover Gateway   |
| x.x.x.128/26 | x.x.x.128 | x.x.x.191 | No Internet Access |

## Assignments

| IP Address                       | Service                 | Notes  |
| -------------------------------- | ----------------------- | ------ |
| x.x.x.1                          | Router                  |        |
| WAN Gateway Access Start         |                         |        |
| x.x.x.2                          | Open Project Proxy      |        |
| x.x.x.3                          | Open Project Web        |        |
| x.x.x.4                          | Plex                    |        |
| x.x.x.5                          | Resilio Sync            |        |
| x.x.x.6                          | Restic B2               |        |
| x.x.x.7                          | Unify Controller        |        |
| x.x.x.8                          | Spotify Server          |        |
| x.x.x.9                          | Spotify Mongo DB        |        |
| x.x.x.10                         | Teslamate Server        |        |
| x.x.x.11                         | Wallabag                |        |
| x.x.x.12                         | Zabbix SNMPTRAPS        |        |
| x.x.x.13                         | Zabbix Server           |        |
| x.x.x.14                         | Zabbix Web NGNIX        |        |
| x.x.x.15                         | Zabbix Agent            |        |
| x.x.x.16                         | Nextcloud AIO           |        |
| x.x.x.17                         | dbt                     |        |
| x.x.x.18                         | meltano                 |        |
| x.x.x.19                         | metabase                |        |
| x.x.x.20                         | Windmill Server         |        |
| x.x.x.21                         | Windmill Worker 1       |        |
| x.x.x.22                         | Windmill Worker 2       |        |
| x.x.x.23                         | Windmill Worker 3       |        |
| x.x.x.24                         | Windmill Worker Native 1|        |
| x.x.x.25                         | Windmill Worker Native 2|        |
| x.x.x.26                         | Lubelog                 |        |
| x.x.x.27                         | Docmost                 |        |
| x.x.x.28                         | Wikijs                  |        |
| x.x.x.29                         | Bookstack               |        |
| x.x.x.30                         | Notesnook server        |        |
| x.x.x.31                         | Notesnook identity      |        |
| x.x.x.32                         | Notesnook SSE           |        |
| x.x.x.33                         | Notesnook S3            |        |
| x.x.x.34                         | Joplin Server           |        |
| x.x.x.35                         | Grist                   |        |
| x.x.x.36                         | IMAP PY                 |        |
| x.x.x.37                         | Immich                  |        |
| x.x.x.38                         | Immich-ml               |        |
| x.x.x.39                         | Tailscale               |        |
| x.x.x.40                         |                         |        |
| x.x.x.41                         |                         |        |
| x.x.x.42                         |                         |        |
| x.x.x.43                         |                         |        |
| x.x.x.44                         |                         |        |
| x.x.x.45                         |                         |        |
| x.x.x.46                         |                         |        |
| x.x.x.47                         |                         |        |
| x.x.x.48                         |                         |        |
| x.x.x.49                         |                         |        |
| x.x.x.50                         | traefik3                |        |
| x.x.x.51                         |                         |        |
| x.x.x.52                         |                         |        |
| x.x.x.53                         |                         |        |
| x.x.x.54                         |                         |        |
| x.x.x.55                         |                         |        |
| x.x.x.56                         |                         |        |
| x.x.x.57                         |                         |        |
| x.x.x.58                         |                         |        |
| x.x.x.59                         |                         |        |
| x.x.x.60                         |                         |        |
| x.x.x.61                         |                         |        |
| x.x.x.62                         |                         |        |
| x.x.x.63                         |                         |        |
| WAN Gateway Access End           |                         |        |
| Vancover Gateway Access Start    |                         |        |
| x.x.x.64                         |                         |        |
| x.x.x.65                         | Sonarr                  |        |
| x.x.x.66                         | Transmission            |        |
| x.x.x.67                         | Jackett                 | Sonarr |
| x.x.x.68                         | Telegram Bot API Server |        |
| x.x.x.69                         |                         |        |
| x.x.x.70                         |                         |        |
| x.x.x.71                         |                         |        |
| x.x.x.72                         |                         |        |
| x.x.x.73                         |                         |        |
| x.x.x.74                         |                         |        |
| x.x.x.75                         |                         |        |
| x.x.x.76                         |                         |        |
| x.x.x.77                         |                         |        |
| x.x.x.78                         |                         |        |
| x.x.x.79                         |                         |        |
| x.x.x.80                         |                         |        |
| x.x.x.81                         |                         |        |
| x.x.x.82                         |                         |        |
| x.x.x.83                         |                         |        |
| x.x.x.84                         |                         |        |
| x.x.x.85                         |                         |        |
| x.x.x.86                         |                         |        |
| x.x.x.87                         |                         |        |
| x.x.x.88                         |                         |        |
| x.x.x.89                         |                         |        |
| x.x.x.90                         |                         |        |
| x.x.x.91                         |                         |        |
| x.x.x.92                         |                         |        |
| x.x.x.93                         |                         |        |
| x.x.x.94                         |                         |        |
| x.x.x.95                         |                         |        |
| x.x.x.96                         |                         |        |
| x.x.x.97                         |                         |        |
| x.x.x.98                         |                         |        |
| x.x.x.99                         |                         |        |
| x.x.x.100                        |                         |        |
| x.x.x.101                        |                         |        |
| x.x.x.102                        |                         |        |
| x.x.x.103                        |                         |        |
| x.x.x.104                        |                         |        |
| x.x.x.105                        |                         |        |
| x.x.x.106                        |                         |        |
| x.x.x.107                        |                         |        |
| x.x.x.108                        |                         |        |
| x.x.x.109                        |                         |        |
| x.x.x.110                        |                         |        |
| x.x.x.111                        |                         |        |
| x.x.x.112                        |                         |        |
| x.x.x.113                        |                         |        |
| x.x.x.114                        |                         |        |
| x.x.x.115                        |                         |        |
| x.x.x.116                        |                         |        |
| x.x.x.117                        |                         |        |
| x.x.x.118                        |                         |        |
| x.x.x.119                        |                         |        |
| x.x.x.120                        |                         |        |
| x.x.x.121                        |                         |        |
| x.x.x.122                        |                         |        |
| x.x.x.123                        |                         |        |
| x.x.x.124                        |                         |        |
| x.x.x.125                        |                         |        |
| x.x.x.126                        |                         |        |
| x.x.x.127                        |                         |        |
| Vancover Gateway Access End      |                         |        |
| No Internet Gateway Access Start |                         |        |
| x.x.x.128                        | Gitea                   |        |
| x.x.x.129                        | Gitea Uploader          |        |
| x.x.x.130                        | Graylog                 |        |
| x.x.x.131                        | InfluxDB HA             |        |
| x.x.x.132                        | Paperless NGX Webserver |        |
| x.x.x.133                        | Postgres HA             |        |
| x.x.x.134                        | Teedy Server            |        |
| x.x.x.135                        | Teslamate Grafana       |        |
| x.x.x.136                        | Restic QNAP             |        |
| x.x.x.137                        | Obsidian Livesync PK    |        |
| x.x.x.138                        | Homebox                 |        |
| x.x.x.139                        | Obsidian Livesync DB    |        |
| x.x.x.140                        | Teslamate Postgres      |        |
| x.x.x.141                        | Grafana HA              |        |
| x.x.x.142                        | Postgres PKDB           |        |
| x.x.x.143                        | airbyte                 |        |
| x.x.x.144                        | dagster                 |        |
| x.x.x.145                        | Windmill Web Proxy      |        |
| x.x.x.146                        | Postgres Meltano        |        |
| x.x.x.147                        | Restic Serv             |        |
| x.x.x.148                        |                         |        |
| x.x.x.149                        |                         |        |
| x.x.x.150                        |                         |        |
| x.x.x.151                        |                         |        |
| x.x.x.152                        |                         |        |
| x.x.x.153                        |                         |        |
| x.x.x.154                        |                         |        |
| x.x.x.155                        |                         |        |
| x.x.x.156                        |                         |        |
| x.x.x.157                        |                         |        |
| x.x.x.158                        |                         |        |
| x.x.x.159                        |                         |        |
| x.x.x.160                        |                         |        |
| x.x.x.161                        |                         |        |
| x.x.x.162                        |                         |        |
| x.x.x.163                        |                         |        |
| x.x.x.164                        |                         |        |
| x.x.x.165                        |                         |        |
| x.x.x.166                        |                         |        |
| x.x.x.167                        |                         |        |
| x.x.x.168                        |                         |        |
| x.x.x.169                        |                         |        |
| x.x.x.170                        |                         |        |
| x.x.x.171                        |                         |        |
| x.x.x.172                        |                         |        |
| x.x.x.173                        |                         |        |
| x.x.x.174                        |                         |        |
| x.x.x.175                        |                         |        |
| x.x.x.176                        |                         |        |
| x.x.x.177                        |                         |        |
| x.x.x.178                        |                         |        |
| x.x.x.179                        |                         |        |
| x.x.x.180                        |                         |        |
| x.x.x.181                        |                         |        |
| x.x.x.182                        |                         |        |
| x.x.x.183                        |                         |        |
| x.x.x.184                        |                         |        |
| x.x.x.185                        |                         |        |
| x.x.x.186                        |                         |        |
| x.x.x.187                        |                         |        |
| x.x.x.188                        |                         |        |
| x.x.x.189                        |                         |        |
| x.x.x.190                        |                         |        |
| x.x.x.191                        |                         |        |
| No Internet Gateway Access End   |                         |        |
| Unassigned Start                 |                         |        |
| x.x.x.192                        |                         |        |
| x.x.x.193                        |                         |        |
| x.x.x.194                        |                         |        |
| x.x.x.195                        |                         |        |
| x.x.x.196                        |                         |        |
| x.x.x.197                        |                         |        |
| x.x.x.198                        |                         |        |
| x.x.x.199                        |                         |        |
| x.x.x.200                        |                         |        |
| x.x.x.201                        |                         |        |
| x.x.x.202                        |                         |        |
| x.x.x.203                        |                         |        |
| x.x.x.204                        |                         |        |
| x.x.x.205                        |                         |        |
| x.x.x.206                        |                         |        |
| x.x.x.207                        |                         |        |
| x.x.x.208                        |                         |        |
| x.x.x.209                        |                         |        |
| x.x.x.210                        |                         |        |
| x.x.x.211                        |                         |        |
| x.x.x.212                        |                         |        |
| x.x.x.213                        |                         |        |
| x.x.x.214                        |                         |        |
| x.x.x.215                        |                         |        |
| x.x.x.216                        |                         |        |
| x.x.x.217                        |                         |        |
| x.x.x.218                        |                         |        |
| x.x.x.219                        |                         |        |
| x.x.x.220                        |                         |        |
| x.x.x.221                        |                         |        |
| x.x.x.222                        |                         |        |
| x.x.x.223                        |                         |        |
| x.x.x.224                        |                         |        |
| x.x.x.225                        |                         |        |
| x.x.x.226                        |                         |        |
| x.x.x.227                        |                         |        |
| x.x.x.228                        |                         |        |
| x.x.x.229                        |                         |        |
| x.x.x.230                        |                         |        |
| x.x.x.231                        |                         |        |
| x.x.x.232                        |                         |        |
| x.x.x.233                        |                         |        |
| x.x.x.234                        |                         |        |
| x.x.x.235                        |                         |        |
| x.x.x.236                        |                         |        |
| x.x.x.237                        |                         |        |
| x.x.x.238                        |                         |        |
| x.x.x.239                        |                         |        |
| x.x.x.240                        |                         |        |
| x.x.x.241                        |                         |        |
| x.x.x.242                        |                         |        |
| x.x.x.243                        |                         |        |
| x.x.x.244                        |                         |        |
| x.x.x.245                        |                         |        |
| x.x.x.246                        |                         |        |
| x.x.x.247                        |                         |        |
| x.x.x.248                        |                         |        |
| x.x.x.249                        |                         |        |
| x.x.x.250                        |                         |        |
| x.x.x.251                        |                         |        |
| x.x.x.252                        |                         |        |
| x.x.x.253                        |                         |        |
| x.x.x.254                        |                         |        |
| x.x.x.255                        |                         |        |
| Unassigned End                   |                         |        |

